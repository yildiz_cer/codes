package com.example.mk.firebase2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    FirebaseDatabase database;
    EditText key , value;
    Button ekle;
    TextView result;
    DatabaseReference ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();

        goster();

        action();


    }

    public void tanimla()
    {
        database = FirebaseDatabase.getInstance();
        key = (EditText)findViewById(R.id.key);
        value = (EditText)findViewById(R.id.value);
        ekle = (Button) findViewById(R.id.ekle);
        result = (TextView)findViewById(R.id.result);
    }

    public void action()
    {
        ekle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyText  = key.getText().toString();
                String valueText = value.getText().toString();
                ref = database.getReference(keyText);
                ref.setValue(valueText);
                key.setText("");
                value.setText("");

            }
        });
    }

    public void goster()

    {
        ref = database.getReference("adi");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                result.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
