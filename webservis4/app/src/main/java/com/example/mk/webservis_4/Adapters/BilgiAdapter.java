package com.example.mk.webservis_4.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mk.webservis_4.Main2Activity;
import com.example.mk.webservis_4.Models.Bilgi;
import com.example.mk.webservis_4.R;

import java.util.List;

/**
 * Created by mk on 04.01.2018.
 */

public class BilgiAdapter extends BaseAdapter {

    List<Bilgi> bilgiList;
    Context context;
    Activity activity ;

    public BilgiAdapter(List<Bilgi> bilgiList, Context context , Activity activity) {
        this.bilgiList = bilgiList;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return bilgiList.size();
    }

    @Override
    public Object getItem(int position) {
        return bilgiList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        TextView postId, id, name, email, body;
        LinearLayout layoutList ;
        layoutList = convertView.findViewById(R.id.layaoutList);
        postId = (TextView) convertView.findViewById(R.id.postId);
        id = (TextView) convertView.findViewById(R.id.id);
        name = (TextView) convertView.findViewById(R.id.name);
        email = (TextView) convertView.findViewById(R.id.email);
        body = (TextView) convertView.findViewById(R.id.body);

        postId.setText("" + bilgiList.get(position).getPostId());
        id.setText("" + bilgiList.get(position).getId());
        name.setText(bilgiList.get(position).getName());
        email.setText(bilgiList.get(position).getEmail());
        body.setText(bilgiList.get(position).getBody());
        final String post = ""+bilgiList.get(position).getPostId();
        final  String idm = ""+bilgiList.get(position).getId();


        layoutList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ıntent = new Intent(activity,Main2Activity.class);
                ıntent.putExtra("post_id",post);
                ıntent.putExtra("id",idm);
                activity.startActivity(ıntent);
            }
        });
        return convertView;
    }
}
