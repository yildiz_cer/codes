package com.example.mk.webservis_4.RestApi;

import com.example.mk.webservis_4.Models.Bilgi;
import com.example.mk.webservis_4.Models.Result;

import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 04.01.2018.
 */

public class ManagerAll extends BaseManager {
    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getIntance() {
        return ourInstance;
    }


    public Call<List<Bilgi>> getirCall() {
        Call<List<Bilgi>> x = getRestApi().getir();
        return x;
    }

    public Call<List<Result>> managerGetResult(String post, String id) {
        Call<List<Result>> y = getRestApi().getirResult(post, id);

        return y;
    }
}
