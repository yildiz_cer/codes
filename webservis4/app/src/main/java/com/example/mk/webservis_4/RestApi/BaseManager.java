package com.example.mk.webservis_4.RestApi;

/**
 * Created by mk on 04.01.2018.
 */

public class BaseManager {

    protected RestApi getRestApi()
    {
        RestApiClient restApiClient = new RestApiClient(BaseUrl.Url);
        return restApiClient.getRestApi();
    }
}
