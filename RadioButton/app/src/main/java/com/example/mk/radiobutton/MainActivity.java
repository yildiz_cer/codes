package com.example.mk.radiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioButton resim1Radio;
    RadioButton resim2Radio;
    RadioButton resim3Radio;
    RadioButton resim4Radio;
    ImageView img;
    RadioGroup radioGroup;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
       // islevVer();
        idAl();
    }

    public void tanimla() {
        resim1Radio = (RadioButton) findViewById(R.id.resim1);
        resim2Radio = (RadioButton)findViewById(R.id.resim2);
        resim3Radio = (RadioButton)findViewById(R.id.resim3);
        resim4Radio = (RadioButton)findViewById(R.id.resim4);
        img = (ImageView)findViewById(R.id.imageview);
        radioGroup =(RadioGroup) findViewById(R.id.radioGrp);
        btn = (Button)findViewById(R.id.btn);
    }


    public void idAl()
    {

      btn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              int gelenId = radioGroup.getCheckedRadioButtonId().;
              resimdegis(gelenId);
          }
      });

    }

    public void islevVer()
    {
        resim1Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img.setImageResource(R.drawable.bir);
                Toast.makeText(getApplicationContext(), resim1Radio.getText(),Toast.LENGTH_LONG).show();
            }
        });
        resim2Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img.setImageResource(R.drawable.iki);
                Toast.makeText(getApplicationContext(), resim2Radio.getText(),Toast.LENGTH_LONG).show();
            }
        });
        resim3Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img.setImageResource(R.drawable.uc);
                Toast.makeText(getApplicationContext(), resim3Radio.getText(),Toast.LENGTH_LONG).show();
            }
        });
        resim4Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img.setImageResource(R.drawable.dort);
                Toast.makeText(getApplicationContext(), resim4Radio.getText(),Toast.LENGTH_LONG).show();
            }
        });
    }

    public void resimdegis(int id)
    {
        if(id == R.id.resim1)
        {
            img.setImageResource(R.drawable.bir);
            Toast.makeText(getApplicationContext(), resim1Radio.getText(),Toast.LENGTH_LONG).show();
        }else if(id == R.id.resim2)
        {
            img.setImageResource(R.drawable.iki);
            Toast.makeText(getApplicationContext(), resim2Radio.getText(),Toast.LENGTH_LONG).show();
        }else if(id == R.id.resim3)
        {
            img.setImageResource(R.drawable.uc);
            Toast.makeText(getApplicationContext(), resim3Radio.getText(),Toast.LENGTH_LONG).show();
        }else if ( id == R.id.resim4)
        {
            img.setImageResource(R.drawable.dort);
            Toast.makeText(getApplicationContext(), resim4Radio.getText(),Toast.LENGTH_LONG).show();
        }
    }
}
