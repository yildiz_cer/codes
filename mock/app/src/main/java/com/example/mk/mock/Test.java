package com.example.mk.mock;

public class Test{
	private String imageUrl;
	private String id;

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Test{" + 
			"imageUrl = '" + imageUrl + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}
