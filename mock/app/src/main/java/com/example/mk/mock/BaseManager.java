package com.example.mk.mock;

/**
 * Created by mk on 02.01.2018.
 */

public class BaseManager {

    protected RestApi getRestpiClient()
    {
        RestApiClient restApiClient = new RestApiClient(BaseUrl.url_bilgi);
        return  restApiClient.getRestApi();
    }
}
