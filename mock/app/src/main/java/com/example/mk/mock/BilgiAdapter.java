package com.example.mk.mock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;



import java.util.List;

/**
 * Created by mk on 02.01.2018.
 */

public class BilgiAdapter extends BaseAdapter {
    List<Test> bilgiList;
    Context context;

    public BilgiAdapter(List<Test> bilgiList, Context context) {
        this.bilgiList = bilgiList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bilgiList.size();
    }

    @Override
    public Object getItem(int position) {
        return bilgiList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        TextView userId = (TextView) convertView.findViewById(R.id.userId);
        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView title = (TextView) convertView.findViewById(R.id.titleGelen);

        userId.setText(userId.getText() + " " + bilgiList.get(position).getId());
        id.setText(id.getText() + " " + bilgiList.get(position).getImageUrl());


        return convertView;
    }
}
