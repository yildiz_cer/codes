package com.example.mk.mailactivasyon.RestApi;


import com.example.mk.mailactivasyon.Models.Result;

import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<Result> ekle(String kullaniadi, String sifre, String mailadres) {
        Call<Result> x = getRestApi().addUser(kullaniadi, sifre, mailadres);
        return x;
    }

    public Call<Result> aktifEt(String mailAdres, String code) {
        Call<Result> x = getRestApi().aktifEt(mailAdres, code);
        return x;
    }
}
