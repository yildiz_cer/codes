package com.example.mk.mailactivasyon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mk.mailactivasyon.Models.Result;
import com.example.mk.mailactivasyon.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    EditText mail , code ;
    Button aktifEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tanimla();
    }


    public  void tanimla()
    {
        mail = (EditText)findViewById(R.id.mailAdresiEditText);
        code = (EditText) findViewById(R.id.codeEditText);
        aktifEt = (Button)findViewById(R.id.aktifEtButon);
        mail.setText(mailAdresiAl());
        aktifEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aktifEt();
            }
        });

    }

    public String mailAdresiAl()
    {
        String mailAdresi;
        Bundle bundle = getIntent().getExtras();
        mailAdresi = bundle.getString("mailAdresi");
        return mailAdresi;
    }
    public void aktifEt()
    {
        String mail , mailIleGelenKod ;
        mail = mailAdresiAl();
        mailIleGelenKod = code.getText().toString();
        Call<Result> aktifEtResponse = ManagerAll.getInstance().aktifEt(mail,mailIleGelenKod);
        aktifEtResponse.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(response.body().result)
                {
                    Intent ıntent = new Intent(Main2Activity.this,MainActivity.class);
                    startActivity(ıntent);
                }else
                {
                    Toast.makeText(getApplicationContext(), "Mail Adresiniz Zaten Aktif Edilmiştir veya hatalı kod girdiniz", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
}
