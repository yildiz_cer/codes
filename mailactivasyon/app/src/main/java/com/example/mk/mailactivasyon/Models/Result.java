package com.example.mk.mailactivasyon.Models;

/**
 * Created by mk on 24.01.2018.
 */

public class Result {
    public boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Result{" +
                "result=" + result +
                '}';
    }
}
