package com.example.mk.mailactivasyon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mk.mailactivasyon.Models.Result;
import com.example.mk.mailactivasyon.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText kullaciAdiEditText, kullaniciSifreEditText, kullaniciMailEditText;
    Button ekleButon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();

    }

    public void tanimla() {
        kullaciAdiEditText = (EditText) findViewById(R.id.kullaniciAdiEditText);
        kullaniciSifreEditText = (EditText) findViewById(R.id.kullaniciSifreEditText);
        kullaniciMailEditText = (EditText) findViewById(R.id.kullaniciMailEditText);
        ekleButon = (Button) findViewById(R.id.ekleButon);
        ekleButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                istek();
            }
        });
    }


    public void istek() {
        final String kullaniciAdi, kullaniciSifre, kullaniciMail;
        kullaniciAdi = kullaciAdiEditText.getText().toString();
        kullaniciSifre = kullaniciSifreEditText.getText().toString();
        kullaniciMail = kullaniciMailEditText.getText().toString();

        if (!kullaniciAdi.equals("") && !kullaniciSifre.equals("") && !kullaniciMail.equals("")) {
            Call<Result> istek = ManagerAll.getInstance().ekle(kullaniciAdi, kullaniciSifre, kullaniciMail);
            istek.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if(response.body().result)
                    {
                        Intent ıntent = new Intent(MainActivity.this,Main2Activity.class);
                        ıntent.putExtra("mailAdresi",kullaniciMail);
                        startActivity(ıntent);
                    }else
                    {
                        Toast.makeText(getApplicationContext(), "Aynı Kullanıcı Adına veya Mail Adresine Sahip bir kayıt var ", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Eksik Bilgi Var", Toast.LENGTH_LONG).show();
        }

    }

}
