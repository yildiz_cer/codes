package com.example.mk.aramayapma;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        aramaYap();


    }

    public  void tanimla()
    {
        editText = (EditText) findViewById(R.id.editTextPhono);
        button = (Button) findViewById(R.id.butonAramaYap);
    }

    public void aramaYap()
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String veriAl = editText.getText().toString();
                Intent ıntent = new Intent(Intent.ACTION_DIAL);
                ıntent.setData(Uri.parse("tel:"+veriAl));

                startActivity(ıntent);
            }
        });
    }


}
