package com.example.mk.firebase4;

/**
 * Created by mk on 25.03.2018.
 */

public class UserDetails {

    public  String isim , soyisim , yas;

    public UserDetails(String isim, String soyisim, String yas) {
        this.isim = isim;
        this.soyisim = soyisim;
        this.yas = yas;
    }
}
