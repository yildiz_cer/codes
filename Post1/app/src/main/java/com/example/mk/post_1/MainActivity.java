package com.example.mk.post_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mk.post_1.Models.Result;
import com.example.mk.post_1.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText ad, soyad;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        add();
    }

    public void tanimla() {
        ad = (EditText) findViewById(R.id.ad);
        soyad = (EditText) findViewById(R.id.soyad);
        btn = (Button) findViewById(R.id.btn);
    }

    public void add() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String isim = ad.getText().toString();
                String soyisim = soyad.getText().toString();
                if(!isim.equals("") && !soyisim.equals("")) {
                    istek(isim, soyisim);
                    deleteFromEdittext();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "boş değer girmeyin", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*
    Intent i = new Intent(FirstScreen.this, SecondScreen.class);
String strName = null;
i.putExtra("STRING_I_NEED", strName);
Then, to retrieve the value try something like:

String newString;
if (savedInstanceState == null) {
    Bundle extras = getIntent().getExtras();
    if(extras == null) {
        newString= null;
    } else {
        newString= extras.getString("STRING_I_NEED");
    }
} else {
    newString= (String) savedInstanceState.getSerializable("STRING_I_NEED");
}
     */

    public void deleteFromEdittext() {
        ad.setText("");
        soyad.setText("");
    }

    public void istek(String ad, String soyad) {
        Call<Result> x = ManagerAll.getInstance().ekle(ad, soyad);
        x.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
}




































