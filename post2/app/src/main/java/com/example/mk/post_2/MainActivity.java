package com.example.mk.post_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mk.post_2.Models.Sonuc;
import com.example.mk.post_2.RestApi.ManagerAll;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText ad, soyad;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        ekle();
    }

    public void tanimla() {
        ad = (EditText) findViewById(R.id.ad);
        soyad = (EditText) findViewById(R.id.soyad);
        btn = (Button) findViewById(R.id.btn);

    }

    public void ekle() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ad.getText().toString().equals("") && !soyad.getText().toString().equals("")) {
                    istek(ad.getText().toString(), soyad.getText().toString());
                    ad.setText("");
                    soyad.setText("");
                }else
                {
                    Toast.makeText(getApplicationContext(), "Alanları Doldurmak Zorunludur...", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void istek(String ad, String soyad) {
        Call<Sonuc> x = ManagerAll.getInstance().ekle(ad, soyad);
        x.enqueue(new Callback<Sonuc>() {
            @Override
            public void onResponse(Call<Sonuc> call, Response<Sonuc> response) {
                Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();
                if (response.body().getResult().equals("Tebrikler Basarıyla Kayit Oldunuz...")) {
                    Intent ıntent = new Intent(MainActivity.this, Main2Activity.class);
                    startActivity(ıntent);
                }
            }

            @Override
            public void onFailure(Call<Sonuc> call, Throwable t) {

            }
        });
    }
}
