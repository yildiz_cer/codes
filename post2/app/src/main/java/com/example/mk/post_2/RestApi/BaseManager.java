package com.example.mk.post_2.RestApi;

/**
 * Created by mk on 07.01.2018.
 */

public class BaseManager {


    protected RestApi getRestApi()
    {
        RestApiClient restApiClient = new RestApiClient(BaseUrl.URL);
        return restApiClient.getRestApi();
    }
}
