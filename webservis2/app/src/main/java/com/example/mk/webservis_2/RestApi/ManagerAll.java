package com.example.mk.webservis_2.RestApi;

import com.example.mk.webservis_2.Models.Bilgi;

import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 02.01.2018.
 */

public class ManagerAll extends BaseManager{

    private static  ManagerAll ourgetInstanse = new ManagerAll();

    public  static synchronized  ManagerAll getInstanse()
    {
        return ourgetInstanse;
    }

    public Call<List<Bilgi>> getirBilgi()
    {
        Call<List<Bilgi>> x = getRestpiClient().getir();
        return x;
    }
}
