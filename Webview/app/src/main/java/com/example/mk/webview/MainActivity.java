package com.example.mk.webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
      //  goster();
        gosterHtml();


    }
    public void tanimla()
    {
        wv = (WebView) findViewById(R.id.webView);


    }
    public void goster()
    {
        wv.getSettings().setJavaScriptEnabled(true); // web sitesindeki javascript kodlarının çalıştırılmasını onaylamak
        wv.loadUrl("https://www.google.com.tr");
    }

    public void gosterHtml()
    {

        String htmlKod = "<html><head>Webview HTML ORNEGI</head><body><h1>HTML ORNEGI</h1></body></html>";
        wv.loadData(htmlKod,"text/html","UTF-8");

    }













}
