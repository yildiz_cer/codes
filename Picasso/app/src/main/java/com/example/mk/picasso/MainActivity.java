package com.example.mk.picasso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    ImageView ımageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        resimCek();
    }
    public void tanimla()
    {
        ımageView = (ImageView)findViewById(R.id.img);
    }

    public  void resimCek()
    {
        Picasso.with(getApplicationContext()).load("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png").into(ımageView);
    }
}
