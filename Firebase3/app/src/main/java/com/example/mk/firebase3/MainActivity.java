package com.example.mk.firebase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference ref1, id, isim, soyisim, yas, durum, x;
    List<User> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        oku("mrtkc");
    }

    public void tanimla() {


        database = FirebaseDatabase.getInstance();
        ref1 = database.getReference("mrtkc");

        ref1.setValue("adasd");

        id = ref1.child("id");
        id.setValue("1");

        isim = ref1.child("isim");
        isim.setValue("murat");

        soyisim = ref1.child("soyisim");
        soyisim.setValue("koç");

        yas = ref1.child("yas");
        yas.setValue(24);

        durum = ref1.child("durum");
        durum.setValue(true);

    }

    public void oku(String anaKey) {

        ref1 = database.getReference(anaKey);
        ref1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("isim", dataSnapshot.getValue().toString());
                User d = dataSnapshot.getValue(User.class);
                if(d.isDurum())
                {
                    Log.i("isim","yaşıyor");
                }else
                {
                    Log.i("isim","öldü");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
