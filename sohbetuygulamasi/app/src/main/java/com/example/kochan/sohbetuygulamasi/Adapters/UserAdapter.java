package com.example.kochan.sohbetuygulamasi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kochan.sohbetuygulamasi.Fragments.OtherProfileFragment;
import com.example.kochan.sohbetuygulamasi.Models.Kullanicilar;
import com.example.kochan.sohbetuygulamasi.R;
import com.example.kochan.sohbetuygulamasi.Utils.ChangeFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    List<String> userKeysList;
    Activity activity;
    Context context;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    public UserAdapter(List<String> userKeysList, Activity activity, Context context) {
        this.userKeysList = userKeysList;
        this.activity = activity;
        this.context = context;
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();
    }

    // layout tanımlaması yapılacak
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.userlayout, parent, false);
        return new ViewHolder(view);
    }

    //viewlara setlemeler yapılacak
    @Override
    public void onBindViewHolder(final @NonNull ViewHolder holder, final int position) {

        // holder.usernameTextView.setText(userKeysList.get(position).toString());
        reference.child("Kullanicilar").child(userKeysList.get(position).toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Kullanicilar kl = dataSnapshot.getValue(Kullanicilar.class);
                Boolean userState = Boolean.parseBoolean(dataSnapshot.child("state").getValue().toString());

                Picasso.get().load(kl.getResim()).into(holder.userimage);
                holder.usernameTextView.setText(kl.getIsim());

                if (userState == true) {
                    holder.user_state_img.setImageResource(R.drawable.online_icon);
                } else {
                    holder.user_state_img.setImageResource(R.drawable.offline_icon);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        holder.userAnaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeFragment fragment = new ChangeFragment(context);
                fragment.changeWithParameter(new OtherProfileFragment(), userKeysList.get(position));
            }
        });


    }

    // adapteri oluşturulacak olan listenın size
    @Override
    public int getItemCount() {
        return userKeysList.size();
    }


    // viewların tanımlanma işlemleri yapılacak
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView usernameTextView;
        CircleImageView userimage,user_state_img;
        LinearLayout userAnaLayout;

        ViewHolder(View itemView) {
            super(itemView);
            usernameTextView = (TextView) itemView.findViewById(R.id.usernameTextView);
            userimage = (CircleImageView) itemView.findViewById(R.id.userimage);
            userAnaLayout = (LinearLayout) itemView.findViewById(R.id.userAnaLayout);
            user_state_img= (CircleImageView) itemView.findViewById(R.id.user_state_img);

        }
    }

}
