package com.example.mk.firebase5;

/**
 * Created by mk on 25.03.2018.
 */

public class UserBilgi {

    public String parola,isim,yas;

    public UserBilgi(String parola, String isim, String yas) {
        this.parola = parola;
        this.isim = isim;
        this.yas = yas;
    }
    public UserBilgi()
    {

    }

    public String getParola() {
        return parola;
    }

    public String getIsim() {
        return isim;
    }

    public String getYas() {
        return yas;
    }
}
