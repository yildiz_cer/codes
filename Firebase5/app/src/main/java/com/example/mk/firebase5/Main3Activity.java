package com.example.mk.firebase5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main3Activity extends AppCompatActivity {
    TextView parolaText,yasText,isimText;

    FirebaseDatabase database;
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        database   = FirebaseDatabase.getInstance();
        ref = database.getReference("users/"+User.getUserName());
        parolaText = (TextView)findViewById(R.id.parolaText);
        yasText = (TextView)findViewById(R.id.yasText);
        isimText = (TextView)findViewById(R.id.isimText);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("gelen",dataSnapshot.getValue().toString());

                UserBilgi userBilgi = dataSnapshot.getValue(UserBilgi.class);

                parolaText.setText(userBilgi.getParola());
                yasText.setText(userBilgi.getYas());
                isimText.setText(userBilgi.getIsim());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
