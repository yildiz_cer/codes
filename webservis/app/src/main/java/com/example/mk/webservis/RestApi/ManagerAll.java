package com.example.mk.webservis.RestApi;


import com.example.mk.webservis.Models.Bilgiler;

import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 30.12.2017.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance()
    {
        return ourInstance;
    }

    public Call<List<Bilgiler>> getirBilgileri()
    {
        Call<List<Bilgiler>> call = getRestApiClient().bilgiGetir();
        return call;
    }
}
