package com.example.mk.webservis.RestApi;

/**
 * Created by mk on 30.12.2017.
 */

public class BaseManager {

    protected RestApi getRestApiClient()
    {
        RestApiClient restApiClient = new RestApiClient(BaseUrl.bilgi_URL);
        return restApiClient.getRestApi();
    }

}
