package com.example.mk.activtylerarasverialma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    EditText kadi,ksifre;
    Button btn ;
    RadioGroup rdGrup;

    String kullaniciAdi,kullaniciSifre,kullaniciCinsiyet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        tiklaveGec();

    }

    void tanimla()
    {

        kadi = (EditText)findViewById(R.id.editTextKullaci);
        ksifre = (EditText)findViewById(R.id.editTextSifre);
        btn = (Button)findViewById(R.id.gınderButton);
        rdGrup = (RadioGroup)findViewById(R.id.CinsiyetGrup);


    }

    void degerAl()
    {
        kullaniciAdi = kadi.getText().toString();
        kullaniciSifre=ksifre.getText().toString();
        kullaniciCinsiyet =  ((RadioButton)findViewById(rdGrup.getCheckedRadioButtonId())).getText().toString();

    }
    void tiklaveGec()
    {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                degerAl();
                Intent ıntent = new Intent(getApplicationContext(),Activity2.class);
                ıntent.putExtra("kullaniciAdi",kullaniciAdi);
                ıntent.putExtra("kullaniciSifre",kullaniciSifre);
                ıntent.putExtra("cinsiyet",kullaniciCinsiyet);
                startActivity(ıntent);

            }
        });
    }





}
