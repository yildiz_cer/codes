package com.example.mk.listview_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    List<mesajModel> liste;
    mesajAdapter adp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        listDoldur();
    }

    public void tanimla()
    {
        listView = (ListView) findViewById(R.id.listview);
    }
    public void listDoldur()
    {
        liste = new ArrayList<>();
        mesajModel m1 = new mesajModel("Merhaba ben Niloya","Niloya",R.drawable.niloya);
        mesajModel m2 = new mesajModel("Merhaba ben murat","murat",R.drawable.murat);
        mesajModel m3 = new mesajModel("Merhaba ben Mete","Mete",R.drawable.mete);
        liste.add(m1);
        liste.add(m2);
        liste.add(m3);
        adp = new mesajAdapter(liste,this);
        listView.setAdapter(adp);

    }

}
