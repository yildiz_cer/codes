package com.example.mk.urunlerlisteleme.RestApi;


import com.example.mk.urunlerlisteleme.Models.Urunler;

import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public  Call<List<Urunler>>  goster() {
        Call<List<Urunler>>  x = getRestApi().listele();
        return x;
    }




}
