package com.example.mk.urunlerlisteleme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.mk.urunlerlisteleme.Adapters.urunAdapter;
import com.example.mk.urunlerlisteleme.Models.Urunler;
import com.example.mk.urunlerlisteleme.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView urunListView;
    List<Urunler> urunList;
    urunAdapter adp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        istekAt();
    }


    public void tanimla() {
        urunListView = (ListView) findViewById(R.id.urunListView);
        urunList = new ArrayList<>();
    }


    public void istekAt() {


        Call<List<Urunler>> istek = ManagerAll.getInstance().goster();

        istek.enqueue(new Callback<List<Urunler>>() {
            @Override
            public void onResponse(Call<List<Urunler>> call, Response<List<Urunler>> response) {
                if (response.isSuccessful()) {
                    urunList = response.body();
                    adp = new urunAdapter(urunList, getApplicationContext());
                    urunListView.setAdapter(adp);
                }
            }

            @Override
            public void onFailure(Call<List<Urunler>> call, Throwable t) {

            }
        });
    }
}
