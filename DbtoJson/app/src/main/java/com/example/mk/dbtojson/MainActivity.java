package com.example.mk.dbtojson;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.mk.dbtojson.Adapters.KullaniciAdapter;
import com.example.mk.dbtojson.Models.Kullanici;
import com.example.mk.dbtojson.Models.Result;
import com.example.mk.dbtojson.RestApi.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    List<Kullanici> kullanicis;
    ListView listView;
    KullaniciAdapter adp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        istek();

    }


    public void tanimla() {
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /*
                DialogClass dialogClass = new DialogClass(MainActivity.this, MainActivity.this);
                dialogClass.goster(kullanicis.get(position).getId());
                Log.i("deneme", "dasdasdsa");
*/
                goster(kullanicis.get(position).getId());

            }
        });
    }

    public void yenile() {
        istek();
    }

    public void istek() {
        kullanicis = new ArrayList<>();
        Call<List<Kullanici>> x = ManagerAll.getInstance().goster();
        x.enqueue(new Callback<List<Kullanici>>() {
            @Override
            public void onResponse(Call<List<Kullanici>> call, Response<List<Kullanici>> response) {
                if (response.isSuccessful()) {
                    kullanicis = response.body();
                    adp = new KullaniciAdapter(kullanicis, getApplicationContext(), MainActivity.this);
                    listView.setAdapter(adp);
                }
            }

            @Override
            public void onFailure(Call<List<Kullanici>> call, Throwable t) {

            }
        });
    }

    public void goster(final String id) {
        LayoutInflater ınflater = getLayoutInflater();
        View view = ınflater.inflate(R.layout.dialoglayout, null);

        Button iptal = (Button) view.findViewById(R.id.iptalButon);
        Button sil = (Button) view.findViewById(R.id.silButon);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog alertDialog = alert.create();
        iptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<Result> sil = ManagerAll.getInstance().deleteFromDb(id);
                sil.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        yenile();
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                    }
                });
                alertDialog.cancel();


            }
        });


        alertDialog.show();
    }


}
