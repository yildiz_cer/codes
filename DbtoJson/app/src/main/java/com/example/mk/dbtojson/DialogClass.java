package com.example.mk.dbtojson;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.mk.dbtojson.Models.Result;
import com.example.mk.dbtojson.RestApi.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mk on 08.01.2018.
 */

public class DialogClass {
    Activity activity;
    Context context;

    public DialogClass(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;

    }

    public void goster(final String id) {
        LayoutInflater ınflater = activity.getLayoutInflater();
        View view = ınflater.inflate(R.layout.dialoglayout, null);

        Button iptal = (Button) view.findViewById(R.id.iptalButon);
        Button sil = (Button) view.findViewById(R.id.silButon);

        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog alertDialog = alert.create();
        iptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<Result> sil = ManagerAll.getInstance().deleteFromDb(id);
                sil.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                    MainActivity mainActivity = new MainActivity();
                    mainActivity.yenile();
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                    }
                });
                alertDialog.cancel();


            }
        });


        alertDialog.show();
    }

}
