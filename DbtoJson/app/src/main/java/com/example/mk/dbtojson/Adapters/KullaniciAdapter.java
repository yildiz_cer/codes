package com.example.mk.dbtojson.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mk.dbtojson.DialogClass;
import com.example.mk.dbtojson.Models.Kullanici;
import com.example.mk.dbtojson.R;

import java.util.List;

/**
 * Created by mk on 08.01.2018.
 */

public class KullaniciAdapter extends BaseAdapter{

    List<Kullanici>  list ;
    Context context;
    Activity activity;



    public KullaniciAdapter(List<Kullanici> list, Context context,Activity activity) {
        this.list = list;
        this.context = context;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout,parent,false);
        TextView id , isim , soyad ;
        LinearLayout linearLayout ;
        id = (TextView)convertView.findViewById(R.id.id);
        isim =(TextView) convertView.findViewById(R.id.isim);
        soyad = (TextView)convertView.findViewById(R.id.soyad);


        id.setText(list.get(position).getId());
        soyad.setText(list.get(position).getSoyad());
        isim.setText(list.get(position).getIsim());


        return convertView;
    }
}
