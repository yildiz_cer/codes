package com.example.mk.dbtojson.Models;

/**
 * Created by mk on 08.01.2018.
 */

public class Result {
    private  String Result;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "Result{" +
                "Result='" + Result + '\'' +
                '}';
    }
}
