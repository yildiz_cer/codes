package com.example.mk.intentkavramveaktivitylerarasgei;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        tiklama();


    }

    public void tanimla() {
        btn = (Button) findViewById(R.id.buton);
    }

    public void gecisYap() {

        Intent ıntent = new Intent(this, Activity2.class);
        startActivity(ıntent);
    }

    public void tiklama() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gecisYap();
            }
        });
    }
}
