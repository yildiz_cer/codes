package com.example.mk.parametreliservisyazma.RestApi;

import com.example.mk.parametreliservisyazma.Models.Uye;
import com.example.mk.parametreliservisyazma.Models.UyeBilgileri;


import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<Uye> giris(String username, String password) {
        Call<Uye> x = getRestApi().addUser(username, password);
        return x;
    }

    public Call<UyeBilgileri> getir(String id) {
        Call<UyeBilgileri> x = getRestApi().bilgiGetir(id);
        return x;
    }
}
