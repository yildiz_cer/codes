package com.example.mk.parametreliservisyazma.RestApi;

import com.example.mk.parametreliservisyazma.Models.Uye;
import com.example.mk.parametreliservisyazma.Models.UyeBilgileri;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mk on 07.01.2018.
 */

public interface RestApi {
    @FormUrlEncoded
    @POST("/girisyap.php")
    Call<Uye> addUser(@Field("username") String username, @Field("password") String password);

    @GET("/bilgigetir.php")
    Call<UyeBilgileri>  bilgiGetir(@Query("uyeid") String id);

}
