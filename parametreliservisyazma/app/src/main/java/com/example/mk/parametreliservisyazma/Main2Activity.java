package com.example.mk.parametreliservisyazma;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mk.parametreliservisyazma.Models.UyeBilgileri;
import com.example.mk.parametreliservisyazma.RestApi.BaseUrl;
import com.example.mk.parametreliservisyazma.RestApi.ManagerAll;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    TextView uyeOkul, uyeMailAdres, uyeIsim, uyeYas;
    ImageView uyeResim;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tanimla();
        idAl();
        istekAt();
    }

    public void idAl() {
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("idDeger");
    }

    public void istekAt() {
        Call<UyeBilgileri> istek = ManagerAll.getInstance().getir(id);
        istek.enqueue(new Callback<UyeBilgileri>() {
            @Override
            public void onResponse(Call<UyeBilgileri> call, Response<UyeBilgileri> response) {
                uyeIsim.setText("Isminiz : " + response.body().getUyeismi());
                uyeMailAdres.setText("Mail Adresiniz" + response.body().getUyemailadres());
                uyeOkul.setText("Okulunuz : " + response.body().getUyeokul());
                uyeYas.setText("Yasınız : " + response.body().getUyeyasi());
                Picasso.with(getApplicationContext()).load(BaseUrl.Url + "/resimler/" + response.body()
                        .getUyeresim()).into(uyeResim);
            }

            @Override
            public void onFailure(Call<UyeBilgileri> call, Throwable t) {

            }
        });
    }

    public void tanimla() {
        uyeOkul = (TextView) findViewById(R.id.uyeOkul);
        uyeMailAdres = (TextView) findViewById(R.id.uyemailAdress);
        uyeIsim = (TextView) findViewById(R.id.uyeIsmi);
        uyeYas = (TextView) findViewById(R.id.uyeYasi);
        uyeResim = (ImageView) findViewById(R.id.uyeResim);
    }
}
