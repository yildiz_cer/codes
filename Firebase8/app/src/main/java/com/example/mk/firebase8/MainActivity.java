package com.example.mk.firebase8;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    DatabaseReference  reference;
    String userId,otherId,key,user,other;
    List<MesajModel> list;
    RecyclerView recyclerView;
    MesajAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new ArrayList<>();
        recyclerView =(RecyclerView) findViewById(R.id.listview);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);
        adapter = new MesajAdapter(list,MainActivity.this);
        recyclerView.setAdapter(adapter);
        reference = FirebaseDatabase.getInstance().getReference();
        userId = "1";
        otherId = "2";
        /*
        sendMessage("selam",userId);
        sendMessage("ne zaman",otherId);
        sendMessage("saat kaçta",userId);
        sendMessage("tamam",otherId);
        sendMessage("ok",userId);
        sendMessage("hangi gün",otherId);
        sendMessage("hangi takimlısın",otherId);
        sendMessage("sanane",userId);
        sendMessage("niye",userId);
        sendMessage("dasdsad",userId);
        sendMessage("hahaha",otherId);
        sendMessage("tamamam",userId);
        */
        load();
    }

    public void sendMessage(String mesajbody,String id)
    {
        user  = "messages/"+userId+"/"+otherId;
        other  = "messages/"+otherId+"/"+userId;
        key = reference.child("messages").child(user).child(otherId).push().getKey();

        Log.i("keyim",key);
        Map map = send(mesajbody,id);
        Map map2 = new HashMap();
        map2.put(user+"/"+key,map);
        map2.put(other+"/"+key,map);

        reference.updateChildren(map2, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

            }
        });


    }

    public Map send(String mesajbody,String userId)
    {
        Map  msj = new HashMap();
        msj.put("mesaj",mesajbody);
        msj.put("from",userId);
        return  msj;
    }

    public void load ()

    {
        reference.child("messages").child(otherId).child(userId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                MesajModel m = dataSnapshot.getValue(MesajModel.class);
                list.add(m);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                MesajModel m = dataSnapshot.getValue(MesajModel.class);
                list.add(m);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                MesajModel m = dataSnapshot.getValue(MesajModel.class);
                list.add(m);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
