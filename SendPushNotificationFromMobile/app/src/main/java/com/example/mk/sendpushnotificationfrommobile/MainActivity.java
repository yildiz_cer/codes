package com.example.mk.sendpushnotificationfrommobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mk.sendpushnotificationfrommobile.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    EditText kadiEditText , ksifreEditText , kmailEditText;
    Button ekleButon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
    }
    public void tanimla()
    {
        kadiEditText=(EditText)findViewById(R.id.kullaniciAdiEditText);
        ksifreEditText=(EditText)findViewById(R.id.kullaniciSifreEditText);
        kmailEditText=(EditText)findViewById(R.id.kullaniciMailEditText);
        ekleButon=(Button)findViewById(R.id.kayitOlButon);
        ekleButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kadi,ksifre,kmail;
                kadi = kadiEditText.getText().toString();
                ksifre = ksifreEditText.getText().toString();
                kmail = kmailEditText.getText().toString();
                kayitOl(kadi,ksifre,kmail);
            }
        });
    }

    public void kayitOl(String ad , String sifre , final String mail)
    {
        Call<Result> request = ManagerAll.getInstance().ekle(ad,sifre,mail);
        request.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {



            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
        Intent ıntent = new Intent(MainActivity.this,Main2Activity.class);
        ıntent.putExtra("mail",mail);
        startActivity(ıntent);
    }
}
