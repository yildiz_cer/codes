package com.example.mk.sendpushnotificationfrommobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mk.sendpushnotificationfrommobile.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    EditText mailEditText , kodEditText;
    Button aktifEtButon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tanimla();
    }
    public void tanimla()
    {
        mailEditText = (EditText)findViewById(R.id.mailEditText);
        kodEditText = (EditText)findViewById(R.id.kodEditText);
        aktifEtButon = (Button) findViewById(R.id.aktifEtButon);
        mailEditText.setText(mailAl());
        aktifEtButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail,kod;
                kod = kodEditText.getText().toString();
                mail = mailEditText.getText().toString();
                istekAt(mail,kod);
            }
        });
    }
    public void istekAt(String mail,String kod)
    {
        Call request = ManagerAll.getInstance().aktifEt(mail,kod);
        request.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });


    }
    public String mailAl()
    {
        String mail;
        Bundle bundle = getIntent().getExtras();
        mail = bundle.getString("mail");
        return mail;
    }
}
