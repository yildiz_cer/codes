package com.example.mk.realm_insert_view;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.security.PublicKey;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    Realm realm;
    EditText kullaniciAdi, sifre, isim;
    Button button, guncelleButon;
    RadioGroup cinsiyetGrup;
    Integer positionT = 0;
    String cinsiyetText, isimText, kullaniciAdiText, sifreText;
    RealmResults<KisiBilgileri> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RealmTanimla();
        tanimla();
        ekle();
        goster();
        pozisyonBul();


    }

    public void RealmTanimla() {
        realm = Realm.getDefaultInstance();
    }

    public void tanimla() {
        listView = (ListView) findViewById(R.id.listview);
        kullaniciAdi = (EditText) findViewById(R.id.editTextKullanici);
        sifre = (EditText) findViewById(R.id.editTextSifre);
        isim = (EditText) findViewById(R.id.editTextIsim);
        cinsiyetGrup = (RadioGroup) findViewById(R.id.CinsiyetRadio);
        button = (Button) findViewById(R.id.kayitOlButon);
        guncelleButon = (Button) findViewById(R.id.guncelleButon);
    }

    public void ekle() {


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                BilgileriAl();

                yaz(cinsiyetText, isimText, kullaniciAdiText, sifreText);
                kullaniciAdi.setText("");
                isim.setText("");
                sifre.setText("");


            }
        });

        guncelleButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                VeriTabanindanListeGetir();

                final KisiBilgileri kisi = list.get(positionT);

                BilgileriAl();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kisi.setCinsiyet(cinsiyetText);
                        kisi.setKullanici(kullaniciAdiText);
                        kisi.setSifre(sifreText);
                        kisi.setIsim(isimText);

                    }
                });
                goster();



            }
        });


    }

    public void yaz(final String cinsiyet, final String isim, final String kullaniciAdi, final String sifre) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                KisiBilgileri kisiBilgileri = realm.createObject(KisiBilgileri.class);
                kisiBilgileri.setCinsiyet(cinsiyet);
                kisiBilgileri.setIsim(isim);
                kisiBilgileri.setKullanici(kullaniciAdi);
                kisiBilgileri.setSifre(sifre);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(), "Başarılı", Toast.LENGTH_LONG).show();
                goster();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Toast.makeText(getApplicationContext(), "başarısız", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void goster() {
        VeriTabanindanListeGetir();
        if (list.size() > 0) {
            adapter adapter = new adapter(list, getApplicationContext());
            listView.setAdapter(adapter);

        }
    }


    public void pozisyonBul() {
        VeriTabanindanListeGetir();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ac(position);
                kullaniciAdi.setText(list.get(position).getKullanici());
                sifre.setText(list.get(position).getSifre());
                isim.setText(list.get(position).getIsim());
                if (list.get(position).getCinsiyet().equals("Erkek")) {
                    ((RadioButton) cinsiyetGrup.getChildAt(0)).setChecked(true);
                } else {
                    ((RadioButton) cinsiyetGrup.getChildAt(1)).setChecked(true);
                }
                positionT = position;
            }
        });
    }

    public void sil(final int position) {
        VeriTabanindanListeGetir();
        Log.i("name", "Liste Eleman Sayısı : " + list.get(position).getKullanici());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                KisiBilgileri kisi = list.get(position);
                kisi.deleteFromRealm();
                goster();


                kullaniciAdi.setText("");
                isim.setText("");
                sifre.setText("");


            }
        });
    }

    public void ac(final int position) {
        LayoutInflater ınflater = getLayoutInflater();
        View view = ınflater.inflate(R.layout.alertlayout, null);

        Button evetButon = (Button) view.findViewById(R.id.evetButon);
        Button hayrButon = (Button) view.findViewById(R.id.hayırButon);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        evetButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sil(position);
                dialog.cancel();
            }
        });
        hayrButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public void BilgileriAl() {
        Integer id = cinsiyetGrup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(id);
        cinsiyetText = radioButton.getText().toString();
        isimText = isim.getText().toString();
        kullaniciAdiText = kullaniciAdi.getText().toString();
        sifreText = sifre.getText().toString();

    }

    public void VeriTabanindanListeGetir() {
        list = realm.where(KisiBilgileri.class).findAll();
    }


}
