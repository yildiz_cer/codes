package com.example.kochan.googlemap6.RestApi;




import com.example.kochan.googlemap6.Models.AnaPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {

    @GET("/maps/api/geocode/json")
    Call<AnaPojo> getInformation(@Query("latlng") String lat);

}


