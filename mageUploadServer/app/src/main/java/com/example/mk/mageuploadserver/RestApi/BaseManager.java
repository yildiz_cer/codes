package com.example.mk.mageuploadserver.RestApi;

/**
 * Created by mk on 21.01.2018.
 */


public class BaseManager {


    protected RestApi getRestApi() {
        RestApiClient restApiClient = new RestApiClient(BaseUrl.Url);
        return restApiClient.getRestApi();
    }
}
