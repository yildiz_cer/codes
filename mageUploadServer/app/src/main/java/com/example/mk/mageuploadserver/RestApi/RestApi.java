package com.example.mk.mageuploadserver.RestApi;

import com.example.mk.mageuploadserver.Models.Result;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by mk on 21.01.2018.
 */

public interface RestApi {
    @FormUrlEncoded
    @POST("/resimekle.php")
    Call<Result> gonder(@Field("baslik") String baslik,@Field("image") String image);

}
