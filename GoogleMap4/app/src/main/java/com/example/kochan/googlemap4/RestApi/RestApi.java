package com.example.kochan.googlemap4.RestApi;



import com.example.kochan.googlemap4.MapPojo;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RestApi {

    @GET("/googlemap.php")
    Call<MapPojo> getMapInformation();

}


