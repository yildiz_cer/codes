package com.example.kochan.googlemap4;

public class MapPojo{
	private String lon;
	private String title;
	private String lat;

	public void setLon(String lon){
		this.lon = lon;
	}

	public String getLon(){
		return lon;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	@Override
 	public String toString(){
		return 
			"MapPojo{" + 
			"lon = '" + lon + '\'' + 
			",title = '" + title + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}
